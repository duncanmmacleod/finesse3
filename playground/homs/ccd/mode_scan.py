import finesse

finesse.plotting.init()

ifo = finesse.Model()
ifo.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=-10
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes maxtem=3

cav FP ITM.p2.o

ccd trns ETM.p2.o xlim=3 ylim=3 npts=300
#ad adnm ETM.p2.o 0

xaxis ITM.phi lin -180 0 150
"""
)

ifo.L0.tem(0, 2, 0.8)
ifo.L0.tem(1, 2, 0.4)
ifo.L0.tem(3, 0, 0.5)
ifo.L0.tem(1, 1, 0.3)

out = ifo.run()
out.plot(cmap="hot")
