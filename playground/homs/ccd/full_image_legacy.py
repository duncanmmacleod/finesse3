import finesse

model = finesse.Model()
model.parse_legacy(
    """
l L0 1 0 n0
s s0 1 n0 nM1
m M1 0.99 0.01 0 nM1 nM2

beam ccd nM1

xaxis ccd x lin -3 3 300
x2axis ccd y lin -3 3 300

gauss* gL0 L0 n0 -2 3
maxtem 4
"""
)

out = model.run()
out.plot()
