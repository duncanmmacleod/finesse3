import finesse

model = finesse.parse(
    """
l L0 P=1
s s0 L0.p1 ITM.p1
m ITM 0.5 0.5 Rc=1934
s s1 ITM.p2 ETM.p1 L=4000
m ETM 0.5 0.5 Rc=2245

cav FP ITM.p2.o ITM.p2.i
modes maxtem=0
"""
)

print(finesse.analysis.beam_trace(model))
