import finesse

model = finesse.Model()
model.parse(
    """
l L0 P=1
s s0 L0.p1 BS.p1 L=10

bs BS

s sY BS.p2 ITMY.p1 L=100

m ITMY R=0.99 T=0.01 Rc=-5580
s LY ITMY.p2 ETMY.p1 L=10k
m ETMY R=0.99 T=0.01 Rc=5580

s sX BS.p3 ITMX.p1 L=50

m ITMX R=0.99 T=0.01 Rc=-5580
s LX ITMX.p2 ETMX.p1 L=10k
m ETMX R=0.99 T=0.01 Rc=5580

cav FPX ITMX.p2.o
cav FPY ITMY.p2.o
gauss gL0 L0.p1.o w0=1m z=0

modes maxtem=0
"""
)

trace = model.beam_trace()
print(model._trace_forest.draw())

# print(trace)
