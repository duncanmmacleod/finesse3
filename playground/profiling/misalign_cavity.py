import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

CODE = """
l L0 1 0 n0

s s0 1 n0 nITM1

m ITM 0.99 0.01 0 nITM1 nITM2
s CAV 1 nITM2 nETM1
m ETM 0.99 0.01 0 nETM1 nETM2

attr ITM Rc -2.5
attr ETM Rc 2.5

cav FP ITM nITM2 ETM nETM1
maxtem 6

xaxis ITM xbeta lin 0 100u 100

pd R nITM1
"""

model = finesse.Model()
model.parse_legacy(CODE)

# out = model.run()

profile()

import pykat

base = pykat.finesse.kat(kat_code=CODE)
base.verbose = False
# out_pykat = base.run()

# import numpy as np
# assert np.allclose(out['R'], out_pykat['R'])

profile("base.run()")
