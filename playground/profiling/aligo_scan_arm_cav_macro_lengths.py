import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

model = finesse.Model()
model.parse_legacy_file("../aligo/design.kat")

model.modes(maxtem=4)

model.elements["LY"].L = model.elements["LX"].L.ref
model.parse("xaxis LX.L lin 3800 4100 100")
model.parse("pd out BSAR2.p3.o")

profile()
