import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

CODE = """
#-----------------------------------------------------------------------
# yaw_modes.kat , test file for kat 0.93
#
# freise@aei.mpg.de  23.07.2002
#
#
# Fabry-Perot Cavity
#
#                                              bs1
#                                              .-.
#                     . . . . . . . . . .      | |
# --->   n1           .      s1         .  n2  | |
#                     . . . . . . . . . .      | |
#                                              | |
#                                              `-'
#----------------------------------------------------------------------


l i1 1 0 n1
gauss g0 i1 n1 870.23867u -5

s s1 10 n1 n2
bs bs1 1 0 0 0 n2 n3 n4 n5
attr bs1 Rc 6

maxtem 9
pd refl n3

phase 0
xaxis bs1 xbeta lin 0 194.591285u 400

yaxis abs
"""

model = finesse.Model()
model.parse_legacy(CODE)

model.modes("x", maxtem=9)
# out = model.run()
# out.plot()
profile()


import pykat

base = pykat.finesse.kat(kat_code=CODE)
base.verbose = False


# profile("base.run()")
