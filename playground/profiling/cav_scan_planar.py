import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

model = finesse.Model()
model.parse(
    """
l L0 P=1 f=0

s s0 L=0 portA=L0.p1 portB=ITM.p1

m ITM (
    R=0.99
    T=0.01
)
s CAV ITM.p2 ETM.p1 L=1
m ETM (
    0.99
    0.01
)

xaxis L0.f lin -100M 100M 2000

pd C ETM.p1.i
"""
)

profile()
