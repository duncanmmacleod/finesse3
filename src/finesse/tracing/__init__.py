"""The beam tracing library of Finesse.

This sub-package includes the core data structures and algorithms used for structuring
and performing beam traces (:mod:`.ctracer`, for developer reference mostly), tools for
executing propagations of beams outside of the context of a simulation (:mod:`.tools`)
and an optimiser which uses these tools (:mod:`.optimise`).
"""
