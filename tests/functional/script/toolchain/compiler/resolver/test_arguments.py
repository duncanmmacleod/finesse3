import pytest
from finesse.script.exceptions import KatScriptError
from ......util import escape_full


@pytest.fixture
def compiler(compiler):
    # Register an element.
    element = lambda a=None, b=None, c=None, d=None: None
    compiler.spec.register_element(
        "fake_element", {"setter": element, "getter": element}
    )
    return compiler


@pytest.mark.parametrize(
    "script,error",
    (
        pytest.param(
            "fake_element myelement a=1 a=2",
            (
                "\nline 1: duplicate arguments with key 'a'\n"
                "-->1: fake_element myelement a=1 a=2\n"
                "                             ^   ^\n"
                "Syntax: fake_element b=none c=none d=none"
            ),
            id="duplicate keyword argument",
        ),
        pytest.param(
            "fake_element myelement a=1 b=2 a=3",
            (
                "\nline 1: duplicate arguments with key 'a'\n"
                "-->1: fake_element myelement a=1 b=2 a=3\n"
                "                             ^       ^\n"
                "Syntax: fake_element b=none c=none d=none"
            ),
            id="duplicate keyword argument, out of order",
        ),
        pytest.param(
            "fake_element myelement a=1 b=2 a=3 b=4",
            (
                "\nline 1: duplicate arguments with keys 'a' and 'b'\n"
                "-->1: fake_element myelement a=1 b=2 a=3 b=4\n"
                "                             ^   ^   ^   ^\n"
                "Syntax: fake_element b=none c=none d=none"
            ),
            id="multiple duplicate keyword arguments",
        ),
    ),
)
def test_duplicate_argument_error(compiler, script, error):
    """Test duplicate arguments get caught in the resolving stage.

    Duplicate arguments are checked in the resolver because later they get added into
    dicts for passing to the setter where duplicate keys would otherwise get silently
    overwritten.
    """
    with pytest.raises(KatScriptError, match=escape_full(error)):
        compiler.compile(script)
