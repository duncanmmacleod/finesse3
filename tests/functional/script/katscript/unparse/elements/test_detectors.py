import pytest
from finesse.components import Mirror
from finesse.detectors import PowerDetector
from finesse.script import unparse
from finesse.script.generator import ElementContainer


names = pytest.mark.parametrize("name", ("pd1", "pd2", "PD0", "_pd_"))
nodes = pytest.mark.parametrize("node", ("m1.p1.i", "m1.p1.o", "m1.p2.i", "m1.p2.o"))


@pytest.fixture
def detector_model(model):
    model.add(Mirror("m1"))
    return model


@names
@nodes
def test_power_detector(detector_model, name, node):
    detector_model.add(PowerDetector(name, detector_model.reduce_get_attr(node)))

    # The detector needs wrapped in a :class:`.ElementContainer` to make it unparse.
    script = unparse(ElementContainer(getattr(detector_model, name)))
    expected = f"power_detector_dc {name} node={node} pdtype=none"
    assert script == expected
