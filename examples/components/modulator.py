import finesse

finesse.plotting.use("default")

model = finesse.Model()
model.parse_legacy_file("modulator.kat")
out = model.run()
out.plot()
