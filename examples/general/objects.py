"""
Example of kat script object syntax
"""
import finesse


kat = finesse.Model()
kat.parse(
    """
        l l1 P=1
        s s1 l1.p1 cc.p1 L=1

        # Here we define an object cavity called "cc" as a compound object
        cc = (
            m m1 R=0.01 L=0
            # Component names don't clash with those in the parent file
            s s1 m1.p2 comp_mirror.p1 L=10

            # Ports, parameters and components can be exported via the
            # export command, with the syntax
            # `export (internal_name external_name)`
            export (m1.p1 p1)
            export (comp_mirror.p2 p2)
            export (&comp_mirror.R R)
            export (&m1 m1)

            # Objects can also be nested arbitrarily
            comp_mirror = (
                m m1 R=0.99 L=0
                s s1 m1.p2 m2.p1 L=0.1 nr=1.45
                m m2 R=0.99 L=0

                # Component names are prefixed with names of the objects they are within
                # This will show up in the plot as "cc_comp_mirror_pCirc"
                pd pCirc m1.p2.o

                export (m1.p1 p1)
                export (m2.p2 p2)
                export (&m2.R R)
            )

            # Note that objects have no knowledge of the parent script, and
            # cannot access variables and components from above
            # l l2 P=&l1.P  # Illegal
        )

        # We could also define this object in a different file by
        # omitting the parentheses, and specifying a filename:
        # cc = cc.kat

        # From outside the object, we can access the exported ports as if cc
        # was a normal object
        pd pTrans cc.p2.o
        pd pRefl cc.p1.o

        # Parameters can also be swept in the same way as usual
        xaxis cc.R 0 1 300 lin
        """
)

out = kat.run()
out.plot(logy=True)
