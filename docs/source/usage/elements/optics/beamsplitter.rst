Beamsplitter
~~~~~~~~~~~~

.. kat:element:: beamsplitter

    Parameters
    ----------

    Listed below are the parameters of the beamsplitter component. Certain parameters
    can be changed during a simulation and some cannot, which is highlighted in the `can
    change during simulation` column. These changeable parameters can be used by actions
    such as ``xaxis`` or ``change``. Those that cannot must be changed before a
    simulation is run.

    .. jupyter-execute::
        :hide-code:

        from finesse.utilities.misc import doc_element_parameter_table
        from finesse.components import Beamsplitter
        doc_element_parameter_table(Beamsplitter)
