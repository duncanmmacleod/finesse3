.. include:: /defs.hrst

.. _requirements_guide:

Adding and modifying dependencies
*********************************

|Finesse| manages its dependencies via `pip <https://pip.pypa.io/en/stable/>`_ using
``requirements`` files. These files work in conjunction with the Conda ``environment``
file to allow ease-of-use through Conda environments, with a single set of requirements
held in the requirements files to allow other package management tools to be used if you
so wish.

There are four requirements files in total:

- `requirements.txt
  <https://git.ligo.org/finesse/finesse3/-/blob/master/requirements.txt>`_ --- core
  packages needed to run |Finesse|.
- `requirements-build.txt
  <https://git.ligo.org/finesse/finesse3/-/blob/master/requirements-build.txt>`_ ---
  packages required to build |Finesse| C extensions.
- `requirements-doc.txt
  <https://git.ligo.org/finesse/finesse3/-/blob/master/requirements-doc.txt>`_ ---
  packages required to build the documentation.
- `requirements-test.txt
  <https://git.ligo.org/finesse/finesse3/-/blob/master/requirements-test.txt>`_ ---
  packages required to run the |Finesse| test suite.
- `requirements-dev.txt
  <https://git.ligo.org/finesse/finesse3/-/blob/master/requirements-dev.txt>`_ --- all
  of the above plus some extra formatting and version control related packages for
  working with |Finesse| source code.

In addition to the above requirements files, |Finesse| uses separate
``install_requires`` and ``extras_require`` keyword arguments in ``setup.py``. This is
based on following the best practices recommended in the `Python packaging guides
<https://packaging.python.org/discussions/install-requires-vs-requirements/>`_.

Adding a package to the requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The process outlined below should be adhered to when adding a package to any of the
requirements categories.

1. Identify the package needed and consider why it is needed in addition to the current
   requirements. If it is a large package or involves many source code changes to
   accommodate it, then submit an issue to the `Issue Tracker
   <https://git.ligo.org/finesse/finesse3/issues>`_ before continuing.

2. Determine the minimum / maximum version of the package required to run the code that
   you are adding / changing. You should verify that tests pass with the additional
   package on your local machine - see :ref:`testing`.

3. Ascertain the category that the requirement falls under - i.e. if it is a package
   needed for a new documentation feature then it should be added to the
   ``requirements-doc.txt`` file.

4. Add the package name *and pinned version* to the relevant requirements file. For
   example, if the package is called ``foo`` and the version you want people to use is
   ``v1.1.0`` then add the line::

    foo == 1.1.0

   to the associated requirements file.

5. Add the package name *without the pinned version* to

   a. the ``REQUIRES`` list in ``setup.py`` if it is a core package required to run
      |Finesse| OR
   b. the ``EXTRAS`` dict in ``setup.py`` if it is any other type of package.

Changing the pinned version of a requirement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Follow the process below to modify the version of an existing requirement.

1. Identify the package version to modify and consider why you need to change the
   required version. If it is a core package used frequently (e.g. ``NumPy``) then
   submit an issue to the `Issue Tracker
   <https://git.ligo.org/finesse/finesse3/issues>`_ before continuing.

2. Determine the version you want to change the requirement to. This can be a minimum
   version, maximum version or pinned at some specific version number.

3. Find the package name in the correct requirements file and modify the version number
   and condition to the value determined from the previous step.  For example, if the
   package is called ``foo`` and the current pinned version is ``v1.1.0`` then the line
   in the requirements file will be::

    foo == 1.1.0

   If you want to change this to a minimum version of ``v1.3.0`` then this line should
   be changed to::

    foo >= 1.3.0

5. Verify that the test suites pass with the package version changed on your local
   machine - see :ref:`testing` - before attempting to merge the changes.

Removing a required package
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Occasionally, we may want to remove a package from the requirements if it is no longer
used (typically when a better solution to some problem has been found using a new /
existing package). In this case, the process outlined here should be followed for
removing the package.

1. Identify the package to remove and consider why it should be removed. If it is a
   large package or involves many source code changes to accommodate it, then submit an
   issue to the `Issue Tracker <https://git.ligo.org/finesse/finesse3/issues>`_ before
   continuing.

2. Ascertain the category that the requirement falls under - i.e. if it was a package
   needed for a feature of |Finesse| itself then it should be removed from the
   ``requirements.txt`` file.

3. Remove the line containing the package name from the relevant requirements file.

4. Delete the line containing the package name from

   a. the ``REQUIRES`` list in ``setup.py`` if it is a core package required to run
      |Finesse| OR
   b. the ``EXTRAS`` dict in ``setup.py`` if it is any other type of package.

5. Verify that the test suites pass with the package removed from your local machine -
   see :ref:`testing` - before attempting to merge the changes.
