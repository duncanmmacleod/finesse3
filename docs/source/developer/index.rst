Developer guide
===============

.. toctree::
    :maxdepth: 3

    get_started/index
    codeguide/index
    packaging
    documenting
    extensions
    code_style
    testing/index
    debugging
