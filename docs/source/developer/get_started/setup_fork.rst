.. include:: ../../defs.hrst

.. _setup_fork:

Setting up your fork
~~~~~~~~~~~~~~~~~~~~

Before reading this you should read steps 1 - 4 in :ref:`how_to_contribute`. After these
steps you will be ready to complete the following process:

1. After cloning, change directory to your local copy of your fork of the |Finesse| 3
   repository::

    cd finesse3

2. Now you want to link your repository to the upstream repository (the main |Finesse| 3
   repo), so that you can fetch changes from trunk. To do this, run::

    git remote add upstream git://git.ligo.org/finesse/finesse3.git

   `upstream` here is an arbitrary name we use to refer to the main |Finesse| 3
   repository. Note the use of ``git://`` for the URL - this is a read-only URL which
   means that you cannot accidentally write to the upstream repository. You can only use
   it to merge into your fork.

3. Check that your remotes are set-up correctly with::

    git remote -v show

   This should give you something similar to::

    upstream    git://git.ligo.org/finesse/finesse.git (fetch)
    upstream    git://git.ligo.org/finesse/finesse.git (push)
    origin      git@git.ligo.org:<username>/finesse3.git (fetch)
    origin      git@git.ligo.org:<username>/finesse3.git (push)
