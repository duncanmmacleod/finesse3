.. include:: ../../defs.hrst
.. _dev_get_started:

Getting started with Finesse development
----------------------------------------

|Finesse| is an open source program where any contributions (of any size) from members
of the optical simulation community are welcome and encouraged.

The project is hosted on `<https://git.ligo.org/finesse/finesse3>`_.

Submitting a bug report
***********************

If you encounter a bug in the code or documentation, especially in a release version, do
not hesitate to submit an issue to the `Issue Tracker
<https://git.ligo.org/finesse/finesse3/issues>`_.

When reporting a bug, please include the following:

- A short, top-level summary of the bug. In most cases, this should be 1-2 sentences.
- A **minimal** but *complete and verifiable* code snippet example to reproduce the bug.
- The actual result(s) of the example provided versus the expected result(s)
- The |Finesse| version, Python version and platform (i.e. OS version) you are using.
  You can find the first two from a Python interpreter with, e.g:

.. jupyter-execute::

    import platform
    import finesse

    print(platform.python_version())
    print(finesse.__version__)

If the bug is Physics-related (e.g. an unexpected discrepancy in results between
|Finesse| and another method) then you should also provide details of the method(s) used
to compute the result - in particular the equations used.

.. _how_to_contribute:

Contributing to the project
***************************

For more in-depth guides on the points in this section see the pages below.

.. toctree::
    :maxdepth: 1

    setup_fork

These pages are also linked when relevant in the numbered list below.

How to contribute
~~~~~~~~~~~~~~~~~

The preferred way to contribute to |Finesse| is to fork the main repository
`<https://git.ligo.org/finesse/finesse3>`_, work on this copy
of the code then submit a "merge request".

A brief overview of the process is given below, with more details to be found in the
sections later on.

1. Log-in to the the `LIGO GitLab server <https://git.ligo.org/users/sign_in>`_ with
   your LIGO / Virgo / KAGRA account.

2. Go to the |Finesse| repository homepage: `<https://git.ligo.org/finesse/finesse3>`_.

3. Click on the "Fork" button near the top of the page. This will create a copy of the
   |Finesse| repository under your account on the GitLab server.

   *For more details on this see* - :ref:`setup_fork`

4. Clone this copy to your local disk, via HTTPS::

    git clone https://git.ligo.org/<username>/finesse3.git

   Or via SSH::

    git clone git@git.ligo.org:<username>/finesse3.git

   Replacing `<username>` with your username on the LIGO GitLab server.

5. Install the developer dependencies via pip::

    pip install -r requirements-dev.txt

6. Install the pre-commit hooks that will check your code for style errors when you
   commit::

    pre-commit install

7. Create a new branch on your local copy to hold the additions / changes you want to
   contribute::

    git checkout -b <feature-name> origin/master

   and start programming! **Note that you should never work in the** `master`
   **branch**.

8. Work on this branch, on your machine, using Git to do the version control. Push any
   changes you have committed to your copy of the |Finesse| repository::

    git push -u origin <feature-name>

9. Once you are happy with the state of your changes, go to the webpage of your fork of
   the |Finesse| repository and click "Merge Request" to send these changes to the
   maintainers of |Finesse| for review.
