.. include:: ../defs.hrst

.. _packaging:

Packaging Finesse
-----------------

|Finesse| is currently packaged for `pip <https://pypi.org/>`_ and `conda
<https://conda.io/>`_. |Finesse| requires the following system dependencies in addition
to those for pip found in ``requirements.txt``:

- Python 3.8 or higher
- SuiteSparse 4.0 or higher

Python 3.8 is a hard requirement due to the use of positional-only arguments, walrus
operators, the use of :py:class:`functools.singledispatchmethod` and the requirement for
``dict`` to be reversible in the code.

Pip
***

Python wheels are built for most Linux platforms (so-called `manylinux` wheels). This
procedure is automated as part of the CI pipeline (see ``.gitlab-ci.yml`` in the
source), but it is quite simple. The Red Hat based Docker image ``manylinux2014``
provided by PyPA is used. The only upstream dependency is currently
``suitesparse-devel`` which provides the KLU C headers used by |Finesse|'s simulation
module.

The procedure for building a `bdist_wheel` for the `manylinux` target is::

    $ yum install -y suitesparse-devel
    $ /path/to/pip install -r requirements-build.txt
    $ /path/to/pip wheel . --no-deps -w wheelhouse
    $ auditwheel repair wheelhouse/*.whl

These commands should be run from the source code root on the Docker image. The
``/path/to/pip`` parts should be replaced by the path to `pip` for the Python version
being targeted.

Conda
*****

.. todo:: Create and document the packaging procedure for Conda.

The conda environment is defined in ``environment.yml`` for most platforms but
``environment-win.yml`` for Windows.
