.. include:: ../defs.hrst
.. _installation:

Installation
------------

For most users we recommend installing the **release version** of |Finesse| as this will
always be the most stable. Any bugs found in release versions can be debugged more
easily than in an arbitrary development version, so subsequent minor releases will
typically address these issues faster.

Release version
***************

|Finesse| 3 is still under active development. There is currently no release version
available. You can find the executables for the stable |Finesse| 2 at:
`<http://www.gwoptics.org/finesse>`_.

From source
***********

.. note::

    If you want to contribute to the development of |Finesse| then you should read
    :ref:`dev_get_started` as this will tell you how to set-up a development environment
    which is separate to any stable version you may have installed.

Follow the steps below to install the latest state of |Finesse|. Make sure to follow the
guide corresponding to the system you are installing to.

Linux / Mac OSX / WSL
~~~~~~~~~~~~~~~~~~~~~

1. If you have not already done so, install the `Conda package manager
   <https://docs.conda.io/en/latest/miniconda.html>`_.

2. Install gcc. If you are on Debian/Ubuntu or similar, install via::

    apt-get install build-essential

   Or if you are on a Mac::

    xcode-select --install

3. Clone the repository `<https://git.ligo.org/finesse/finesse3>`_ via HTTPS::

    git clone https://git.ligo.org/finesse/finesse3

   Or via SSH::

    git clone git@git.ligo.org:finesse/finesse3.git

4. Change directory to your local copy of the |Finesse| repository::

    cd finesse3

5. Create a new Conda environment using the |Finesse| environment file::

    conda env create -n <name_of_environment> -f environment.yml

   where you should substitute ``<name_of_environment>`` for a suitable name of your
   choice, e.g: ``finesse3``.

6. The above step should automatically activate your new environment, but to make sure
   that your shell is using this Conda environment from above run::

    conda activate <name_of_environment>

7. Installation of |Finesse| in development mode is now simple, just run::

    pip install -e .

   in the root directory of your local copy of the |Finesse| repository.

Windows
~~~~~~~

1. If you have not already done so, install the `Conda package manager
   <https://docs.conda.io/en/latest/miniconda.html>`_.

2. Again, if you have not already done so, install `Git for Windows
   <https://gitforwindows.org/>`_. You can either use the tools this provides directly
   to clone the |Finesse| repository (see step 5 below) or add the path to your git
   executable to the Windows environment variables - details on how to do this are shown
   in the next step, but you can skip this if you intend to use the *git bash/gui here*
   feature(s) of Git for Windows.

3. (Optional) To be able to use git from the command line, and via an Anaconda Prompt,
   you need to add the path to your git executable (installed in the step above) to the
   Windows environment variables.

   Type "environment variables" into the Windows search bar and open the first result.
   Click the "Environment Variables..." button.

   In the new window that pops up, navigate to the lower panel (System Variables) and
   find the field named "Path". Click the "Edit" button with this field highlighted. You
   will now be presented with a window showing all the currently stored paths. Click the
   "New" button in this Window and add the following two paths separately::

    C:\Program Files\Git\bin\git.exe
    C:\Program Files\Git\cmd

   Note that these paths above are assuming that you did not change the default install
   location of Git for Windows in step 2.

4. Open an Anaconda Prompt using the Windows search bar.

5. Clone the repository `<https://git.ligo.org/finesse/finesse3>`_ via HTTPS::

    git clone https://git.ligo.org/finesse/finesse3

   Or via SSH::

    git clone git@git.ligo.org:finesse/finesse3.git

   Note that if you skipped step 3 then you will need to use *git bash here* or *git gui
   here* by right clicking in the File Explorer. If you select *git bash here* then you
   will be presented with a bash-like terminal in which you can run the git clone
   command above. Close this terminal after cloning the repository and return to your
   Anaconda Prompt.

6. Change directory to your local copy of the |Finesse| repository::

    cd finesse3

7. Create a new Conda environment using the |Finesse| **Windows** environment file::

    conda env create -n <name_of_environment> -f environment_win.yml

   where you should substitute ``<name_of_environment>`` for a suitable name of your
   choice, e.g: ``finesse3``.

8. The above step should automatically activate your new environment, but to make sure
   that your shell is using this Conda environment from above run::

    conda activate <name_of_environment>

9. To build the Cython extensions of |Finesse| you must now run::

    make.bat

10. Installation of |Finesse| in development mode is now simple, just run::

     pip install -e .

    in the root directory of your local copy of the |Finesse| repository.

If you encounter any issues here then refer to the troubleshooting guide below.

Note that in the final step of both guides above we use the ``-e`` option with pip
install. This options means that the install is performed in "editable" mode such that
any changes to Python source code files in |Finesse| (i.e. after pulling the latest
changes on master from the repository) will be reflected in a version installed in this
way.

Keeping your development version up to date
*******************************************

If you want to use all the new features of |Finesse| as they are developed then you will
need to pull changes from the repository as and when you need them. To do this, simply
run::

    git pull

when in any directory of your local copy of the |Finesse| repository. If the changes
include any modifications to (or additions / deletions of) ``.pyx`` / ``.pxd`` files
then you will need to re-build the Cython extensions. On Linux / MacOS this means just
repeating step 6 whilst on Windows you should repeat step 9, from the above guides.

Troubleshooting installation from source on Windows
***************************************************

A number of different errors may occur when attempting to install |Finesse| from source
on Windows. The list below should be a comprehensive guide to addressing these issues.

- **I'm getting the following error during the compilation process for the Cython
  extensions:** ``cannot find -lmsvcr140``.

Here are the steps to resolve this error:

1. Use the Windows search bar to find a ``vcruntime140.dll`` file. Open the folder
   location of this file and copy the file.
2. Paste this file to: ``C:\<ANACONDA_PATH>\envs\<ENV_NAME>\Library\mingw-w64\lib``
   where ``<ANACONDA_PATH>`` is the path to your Conda installation and ``<ENV_NAME>``
   is the name of your Conda environment that you are using for your |Finesse|
   installation.
3. If you still get this error after the steps above, then use the Windows search bar
   to find the file ``cygwinccompiler.py``. Open this file and edit the following lines,
   from this::

    elif int(msc_ver) >= 1900:
        # VS2015 / MSVC 14.0
        return ['msvcr140']

   to this::

    elif int(msc_ver) >= 1900:
        # VS2015/ MSVC 14.0
        return ['vcruntime140']
